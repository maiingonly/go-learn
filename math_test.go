package go_learn

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAbsolute(t *testing.T) {

	t.Run("negative test case", func(t *testing.T) {
		x := Absolute(-5)
		// traditional way
		// if x != 5 {
		// 	t.Logf("expect 5, but got %d", x)
		// 	t.Fail()
		// }

		// t.Skip("gausah di test udah jago")

		// t.Fail()

		//with package testify/assert
		assert.Equal(t, 5, x)
	})

	t.Run("positive test case", func(t *testing.T) {
		x := Absolute(5)
		assert.Equal(t, 5, x)
	})
}

/// test table
func TestTable(t *testing.T) {
	testCases := []struct {
		name     string
		a, b     int
		expected int
	}{
		{name: "negative and negative",
			a:        -1,
			b:        -1,
			expected: -2,
		},
		{name: "negative and positive",
			a:        -1,
			b:        3,
			expected: 2,
		},
		{name: "positive and positive",
			a:        5,
			b:        5,
			expected: 10,
		},
	}

	for _, tc := range testCases {
		// t.Run() make sense notif me where test case is fail
		t.Run(tc.name, func(t *testing.T) {
			c := Add(tc.a, tc.b)
			assert.Equal(t, tc.expected, c)
		})

	}
}

func TestAdd(t *testing.T) {
	t.Run("negative and negative", func(t *testing.T) {
		c := Add(-1, -2)
		assert.Equal(t, -3, c)
	})

	t.Run("positive and negative", func(t *testing.T) {
		c := Add(1, -2)
		assert.Equal(t, -1, c)
	})
}
